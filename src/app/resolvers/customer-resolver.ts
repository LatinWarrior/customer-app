import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ICustomer } from './../models/customer';
import { DataService } from './../services/data-service';
import { LoggerService } from './../services/logger-service';

@Injectable()
export class CustomerResolver implements Resolve<ICustomer> {

    /**
     * Constructor.
     */
    constructor(
        private customerService: DataService,
        private logger: LoggerService,
        private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICustomer> {

        const id = route.params['id'];

        if (isNaN(id)) {
            this.logger.log(`Customer id was not a number: ${id}`);
            this.router.navigate(['/customers']);
            return Observable.of(null);
        }

        return this.customerService
            .getCustomer(+id)
            .map(customer => {
                if (customer) {
                    return customer;
                }
                this.logger.log(`Customer was not found: ${id}`);
                this.router.navigate(['/customers']);
                return null;
            })
            .catch(error => {
                this.logger.log(`Retrieval error: ${error}`);
                this.router.navigate(['/customers']);
                return Observable.of(null);
            });
    }

}
