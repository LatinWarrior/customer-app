import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { IState } from './../models/state';
import { DataService } from './../services/data-service';
import { LoggerService } from './../services/logger-service';

@Injectable()
export class StatesResolver implements Resolve<Observable<Array<IState>>> {

    /**
     * Constructor.
     */
    constructor(
        private stateService: DataService,
        private router: Router) {
    }

    resolve(): Observable<Array<IState>> {
        return this.stateService.getStates()
            .map((states) => {
                if (states) {
                    return states;
                };
                console.log(`States were not found.`);
                this.router.navigate(['/customers']);
                return null;
            })
            .catch((error) => {
                console.log(`Retrieval error: ${error}`);
                this.router.navigate(['/customers']);
                return Observable.of(null);
            });
    }

}
