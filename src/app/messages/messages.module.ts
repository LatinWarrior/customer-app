import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; // this is required for ngForm to work.
import { MessagesComponent } from './messages.component';

@NgModule({
  imports: [
    CommonModule // NgForm
  ],
  declarations: [MessagesComponent]
})
export class MessagesModule { }
