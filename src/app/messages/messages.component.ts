import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MessageService } from './../services/message-service';

@Component({
  moduleId: module.id,
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  constructor(private messageService: MessageService,
    private router: Router) { }

  ngOnInit() {
  }

  close(): void {
    // Close the popup.
  }

}
