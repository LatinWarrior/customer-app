import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// import { NgForm } from '@angular/forms';

import { CustomerListComponent } from './customers/customer-list/customer-list.component';
import { CustomerDetailComponent } from './customers/customer-detail/customer-detail.component';
import { CustomerEditComponent } from './customers/customer-edit/customer-edit.component';
import { UserComponent } from './users/user/user.component';
import { MessagesComponent } from './../messages/messages.component';
import { LoginComponent } from './users/login/login.component';

// import { DataService } from './../services/data-service';
import { CustomerResolver } from './../resolvers/customer-resolver';
import { StatesResolver } from './../resolvers/states-resolver';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Google Map
import { AgmCoreModule } from '@agm/core';

import { GridModule } from '@progress/kendo-angular-grid';
import { TabStripModule } from '@progress/kendo-angular-layout';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { CustomerEditBasicComponent } from './customers/customer-edit/tabs/customer-edit-basic/customer-edit-basic.component';
import { CustomerEditAddrHomeComponent } from './customers/customer-edit/tabs/customer-edit-addr-home/customer-edit-addr-home.component';
import { CustomerEditAddrWorkComponent } from './customers/customer-edit/tabs/customer-edit-addr-work/customer-edit-addr-work.component';
import { CompanyMapComponent } from './maps/company-map/company-map.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'customers',
                children: [
                    {
                        path: '',
                        component: CustomerListComponent
                    },
                    {
                        path: ':id',
                        component: CustomerDetailComponent,
                        resolve: { customer: CustomerResolver }
                    },
                    {
                        path: ':id/edit',
                        component: CustomerEditComponent,
                        resolve: { customer: CustomerResolver, stateList: StatesResolver },
                        children: [
                            { path: '', redirectTo: 'basic', pathMatch: 'full' },
                            { path: 'basic', component: CustomerEditBasicComponent },
                            { path: 'addressHome', component: CustomerEditAddrHomeComponent },
                            { path: 'addressWork', component: CustomerEditAddrWorkComponent },
                        ]
                    }
                ]
            },
            { path: 'login', component: LoginComponent },
            { path: 'map', component: CompanyMapComponent }
        ]),
        GridModule,
        TabStripModule,
        DropDownsModule,
        NgbModule,
        CommonModule,
        FormsModule,
        AgmCoreModule
    ],
    declarations: [
        CustomerListComponent,
        CustomerDetailComponent,
        CustomerEditComponent,
        UserComponent,
        LoginComponent,
        CustomerEditBasicComponent,
        CustomerEditAddrHomeComponent,
        CustomerEditAddrWorkComponent,
        CompanyMapComponent
    ],
    providers: [
        CustomerResolver,
        StatesResolver
    ]
})
export class FeaturesModule { }
