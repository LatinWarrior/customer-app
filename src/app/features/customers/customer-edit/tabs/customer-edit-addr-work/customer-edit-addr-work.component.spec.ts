import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerEditAddrWorkComponent } from './customer-edit-addr-work.component';

describe('CustomerEditAddrWorkComponent', () => {
  let component: CustomerEditAddrWorkComponent;
  let fixture: ComponentFixture<CustomerEditAddrWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerEditAddrWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerEditAddrWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
