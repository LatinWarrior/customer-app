import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerEditBasicComponent } from './customer-edit-basic.component';

describe('CustomerEditBasicComponent', () => {
  let component: CustomerEditBasicComponent;
  let fixture: ComponentFixture<CustomerEditBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerEditBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerEditBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
