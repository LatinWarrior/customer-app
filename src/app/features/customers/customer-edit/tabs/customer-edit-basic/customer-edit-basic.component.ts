import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ICustomer } from './../../../../../models/customer';

@Component({
  moduleId: module.id,
  selector: 'app-customer-edit-basic',
  templateUrl: './customer-edit-basic.component.html',
  styleUrls: ['./customer-edit-basic.component.css']
})
export class CustomerEditBasicComponent implements OnInit {

  @ViewChild(NgForm) productForm: NgForm;

  pageTitle: string;
  errorMessage: string;
  customer: ICustomer;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.parent.data.subscribe((data) => {
      this.customer = data['customer'];

      if (this.productForm) {
        this.productForm.reset();
      }
    });
  }

}
