import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { ICustomer } from './../../../../../models/customer';
import { IState } from './../../../../../models/state';

@Component({
  moduleId: module.id,
  selector: 'app-customer-edit-addr-home',
  templateUrl: './customer-edit-addr-home.component.html',
  styleUrls: ['./customer-edit-addr-home.component.css']
})
export class CustomerEditAddrHomeComponent implements OnInit {

  pageTitle: string;
  errorMessage: string;
  @Input() customer: ICustomer;
  stateList: Array<string>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.parent.data.subscribe((data) => {
      this.customer = data['customer'];
      // We are only interested in the state names.
      this.stateList = (data['stateList'] as Array<IState>).map(s => s.name);
    });
  }

}
