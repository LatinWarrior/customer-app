import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerEditAddrHomeComponent } from './customer-edit-addr-home.component';

describe('CustomerEditAddrHomeComponent', () => {
  let component: CustomerEditAddrHomeComponent;
  let fixture: ComponentFixture<CustomerEditAddrHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerEditAddrHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerEditAddrHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
