import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Kendo UI
import { TabStripComponent } from '@progress/kendo-angular-layout';

// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ICustomer } from './../../../models/customer';
import { IState } from './../../../models/state';
import { DataService } from './../../../services/data-service';
import { LoggerService } from '../../../services/logger-service';
import { MessageService } from '../../../services/message-service';

@Component({
  moduleId: module.id,
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {

  pageTitle: string;
  errorMessage: string;
  @Input() customer: ICustomer;
  states: Array<IState>;
  stateList: Array<string>;
  private dataIsValid: { [key: string]: boolean } = {};

  constructor(private customerService: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private logger: LoggerService,
    private messageService: MessageService) { }

  ngOnInit() {
    // Use observable here, instead of snapshot.
    this.customer = this.route.snapshot.data['customer'];
    this.onCustomerRetrieved(this.customer);

    // this.route.parent.data.subscribe((data) => {
    //   debugger;
    //   this.customer = data['customer'];
    //   this.onCustomerRetrieved(this.customer);
    // });

    this.customerService.getStates().subscribe((stateList) => {
      this.stateList = stateList.map(s => s.name);
    });
  }

  onCustomerRetrieved: (customer: ICustomer) => void = (customer: ICustomer) => {

    this.customer = customer;

    if (this.customer && this.customer.id !== 0) {
      this.pageTitle = `Edit Customer: ${this.customer.lastName}, ${this.customer.firstName}`;
    } else {
      this.pageTitle = 'Add Customer';
    }
    // console.log('pageTitle: ', this.pageTitle);
  }

  cancel: () => void = () => {
    // Route back to customer view?
  }

  save: () => void = () => {

    // if (!customer) {
    //   const customerErrorMessage = `customer: ${JSON.stringify(customer)} was NOT saved. The customer model is not present.`;
    //   this.logger.log(customerErrorMessage);
    //   this.errorMessage = customerErrorMessage;
    //   this.messageService.addMessage(this.errorMessage);
    //   return;
    // }

    // if (!isValid) {
    //   const validationErrorMessage = `customer: ${JSON.stringify(customer)} was NOT saved. The form is not valid.`;
    //   this.logger.log(validationErrorMessage);
    //   this.errorMessage = validationErrorMessage;
    //   this.messageService.addMessage(this.errorMessage);
    //   return;
    // }

    // Save customer, stay put.
    this.customerService.saveCustomer(this.customer).subscribe(
      () => {
        const saveSuccessMessage = `customer: ${JSON.stringify(this.customer)} was saved.`;
        this.logger.log(saveSuccessMessage);
        this.onSaveComplete(false, saveSuccessMessage);
      },
      (error: any) => {
        this.logger.log(`customer: ${JSON.stringify(this.customer)} was NOT saved.`);
        this.errorMessage = <any>error;
      }
    );
  }

  onSaveComplete: (navigateToList?: boolean, message?: string) => void = (navigateToList?: boolean, message?: string) => {
    if (message) {
      this.messageService.addMessage(message);
    }

    // Navigate to the customers list, if desired.
    if (navigateToList) {
      this.router.navigate(['/customers']);
    }
  }

  validate: () => void = () => {

    // Clear the validation object.
    this.dataIsValid = {};

    // Basic tab
    if (this.customer.lastName &&
      this.customer.lastName.length &&
      this.customer.firstName &&
      this.customer.firstName.length) {
      this.dataIsValid['basic'] = true;
    } else {
      this.dataIsValid['basic'] = false;
    }

    // Home Address tab

    // Work Address tab

    // Phone tab
  }

  isValid: (path: string) => boolean = (path: string) => {

    this.validate();

    if (path) {
      return this.dataIsValid[path];
    }

    return (this.dataIsValid && Object.keys(this.dataIsValid).every(d => this.dataIsValid[d]));
  }

}
