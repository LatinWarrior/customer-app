import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

// Kendo UI
import { GridModule, GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor, State } from '@progress/kendo-data-query';

// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ICustomer } from '../../../models/customer';
import { CustomerResolver } from './../../../resolvers/customer-resolver';

import { DataService } from '../../../services/data-service';
import { LoggerService } from '../../../services/logger-service';

@Component({
  moduleId: module.id,
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  // http://www.telerik.com/kendo-angular-ui/components/grid/hierarchy/

  public customer: ICustomer;
  public customers: Array<ICustomer>;
  public saveCustomers: Array<ICustomer>;
  private isBusy = false;
  public totalCount: number;
  public pageSize: number;
  public currentPage: number;
  public pageCount: number;
  public gridData: any;
  public skip: number;
  public sort: Array<SortDescriptor> = [];
  public gridView: GridDataResult;
  public selectedCustomer: ICustomer;
  private pageSizeDefault: number = 10;

  constructor(
    private dataService: DataService,
    private logger: LoggerService,
    private router: Router,
    private resolver: CustomerResolver) {

  }

  ngOnInit() {
    this.getCustomers();
  }

  public selectCustomer: (cust: any) => void = (cust: any) => {
    this.logger.log(`customer.id: ${cust.id}`);
  }

  public dataStateChange: ({ skip, take, sort }: DataStateChangeEvent) => void = ({ skip, take, sort }: DataStateChangeEvent) => {
    // Save the current state of the Grid component
    this.currentPage = skip;
    this.pageSize = take;
    this.sort = sort;

    // Reload the data with the new state
    this.getNextPage(this.currentPage, this.pageSize, this.sort);
  }

  previousPage: () => void = () => {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  nextPage: () => void = () => {
    if (this.currentPage < this.totalCount) {
      this.currentPage++;
    }
  }

  public removeCustomer(customer: ICustomer) {
    this.logger.log(`In removeCustomer. customer: ${customer}`);
  }

  public editCustomer(customer: ICustomer) {
    this.selectedCustomer = customer;
    this.logger.log(`In editCustomer. customer: ${customer}`);
    this.router.navigate(['/customers', this.selectedCustomer.id, 'edit']);
  }

  public viewCustomer(customer: ICustomer) {
    this.selectedCustomer = customer;
    this.logger.log('In viewCustomer. customer: ' + JSON.stringify(customer));
    this.router.navigate(['/customers', this.selectedCustomer.id]);
  }

  public getNextPage(skip?: number, take?: number, sort?: Array<SortDescriptor>) {
    this.pageSize = take || this.pageSizeDefault;
    this.currentPage = skip || 0;
    this.pageCount = 0;

    this.gridView = {
      data: this.saveCustomers.slice(this.currentPage, this.currentPage + this.pageSize),
      total: this.saveCustomers.length
    };
  }

  public getCustomers(skip?: number, take?: number, sort?: Array<SortDescriptor>) {

    this.customer = undefined;  // <-- clear before refresh
    this.customers = undefined;
    this.totalCount = 0;
    this.pageSize = take || this.pageSizeDefault;
    this.currentPage = skip || 0;
    this.pageCount = 0;

    this.isBusy = true;
    this.logger.log('Getting customers ...');

    // this.dataService.getCustomersP().then(  // Promise version
    this.dataService.getCustomers().subscribe( // Observable version
      customers => {
        this.isBusy = false;
        this.saveCustomers = customers;
        this.customers = this.saveCustomers.slice(0, this.pageSize);
        this.totalCount = this.saveCustomers.length;
        this.currentPage = 1;
        this.pageCount = Math.ceil(this.totalCount / this.pageSize);
        this.gridData = this.saveCustomers;

        this.gridView = {
          data: this.saveCustomers.slice(0, this.pageSize),
          total: this.saveCustomers.length
        };
      },
      (errorMsg: string) => {
        this.isBusy = false;
        // alert(errorMsg); // Don't use alert!
        this.logger.log(errorMsg);
      }
    );
  }

  save(customer: ICustomer) {
    if (!customer) { return; }
    this.isBusy = true;
    this.logger.log(`Saving ${customer.lastName} ...`);
    this.dataService.update(customer).subscribe(
      () => this.isBusy = false,
      () => {
        this.isBusy = false;
        this.logger.log('Save failed; please check the console');
        // alert('Save failed; please check the console'); // Don't use alert!
      }
    );
  }

  shift(increment: number) {
    let ix = increment + this.customers.findIndex(c => c === this.customer);
    ix = Math.min(this.customers.length - 1, Math.max(0, ix));
    this.customer = this.customers[ix];
  }

}
