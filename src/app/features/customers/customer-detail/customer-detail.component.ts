import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ICustomer } from '../../../models/customer';

import { LoggerService } from '../../../services/logger-service';

@Component({
  moduleId: module.id,
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {

  private id: number;
  private customers: Array<ICustomer>;
  public pageTitle: string;

  @Input() customer: ICustomer;

  constructor(
    private logger: LoggerService,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.customer = this.route.snapshot.data['customer'];
  }

}
