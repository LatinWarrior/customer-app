import { Component, OnInit } from '@angular/core';

// Google Map
import { AgmCoreModule } from '@agm/core';
import {
  AgmDataLayer, AgmMarker, AgmMap, AgmCircle, AgmPolygon, AgmPolyline,
  AgmInfoWindow, AgmKmlLayer, AgmPolylinePoint
} from '@agm/core/directives';
import {
  DataMouseEvent, LatLngLiteral, MouseEvent, KmlMouseEvent, LatLngBounds,
  LatLngBoundsLiteral, PolyMouseEvent
} from '@agm/core/map-types';
import {
  CircleManager, DataLayerManager, GoogleMapsAPIWrapper, InfoWindowManager,
  GoogleMapsScriptProtocol, KmlLayerManager, MarkerManager, LazyMapsAPILoader,
  LAZY_MAPS_API_CONFIG, LazyMapsAPILoaderConfigLiteral, MapsAPILoader,
  NoOpMapsAPILoader, PolygonManager, PolylineManager
} from '@agm/core/services';

import { IMarker } from './../../../models/marker';

@Component({
  moduleId: module.id,
  selector: 'app-company-map',
  templateUrl: './company-map.component.html',
  styleUrls: ['./company-map.component.css']
})
export class CompanyMapComponent implements OnInit {

  // Google maps zoom level
  zoom: number = 8;

  // Initial center position for the map
  lat: number = 51.673858;
  lng: number = 7.815982;

  markers: Array<IMarker>;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    const marker: IMarker = {
      lat: ($event as any).coords.lat,
      lng: ($event as any).coords.lng,
      label: null,
      draggable: true
    };
    this.markers.push(marker);
  }

  markerDragEnd(m: IMarker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  constructor() { }

  ngOnInit() {

    this.markers = [
      {
        lat: 51.673858,
        lng: 7.815982,
        label: 'A',
        draggable: true
      },
      {
        lat: 51.373858,
        lng: 7.215982,
        label: 'B',
        draggable: false
      },
      {
        lat: 51.723858,
        lng: 7.895982,
        label: 'C',
        draggable: true
      }
    ];

  }

}
