// Observable DataService
import { Injectable } from '@angular/core';
import { Response, Http, Headers, RequestOptions } from '@angular/http';  // <-- import Http & Headers

import { LoggerService } from './logger-service';
import { ICustomer } from '../models/customer';
import { IState } from '../models/state';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch'; // <-- add rxjs operator extensions used here
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import 'rxjs/add/observable/throw'; // <-- add rxjs Observable extensions used here

@Injectable()
export class DataService {
  private customersUrl = 'api/customers';
  private productsUrl = 'api/products';
  private statesUrl = 'api/states';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private http: Http,  // <-- inject http
    private logger: LoggerService) { }

    getStates(): Observable<Array<IState>>{
      return this.http.get(this.statesUrl)
      .map(this.extractData)
      // .do(data => console.log('getCustomers: ' + JSON.stringify(data)))
      .catch(this.handleError);
    }

  /** Get existing customers as an Observable */
  getCustomers(): Observable<Array<ICustomer>> {
    // this.logger.log('Getting customers as an Observable via Http ...');

    return this.http.get(this.customersUrl)
      .map(this.extractData)
      // .do(data => console.log('getCustomers: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getCustomer(id: number): Observable<ICustomer> {
    // this.logger.log(`Getting customer, by id: ${id}, as an Observable via Http...`);

    if (id === 0) {
      return Observable.of(this.initializeCustomer());
    };
    const url = `${this.customersUrl}/${id}`;
    return this.http.get(url)
      .map(this.extractData)
      // .do(data => console.log('getCustomer: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  /** Update existing customer */
  update(customer: ICustomer): Observable<any> {
    const url = `${this.customersUrl}/${customer.id}`;
    const result = this.http.put(url, customer, { headers: this.headers })
      .do(response => this.logger.log(`Saved customer ${customer.id}`))
      .share(); // execute once no matter how many subscriptions

    // Result is "cold" which means the update won't happen until something subscribes
    // Ensure update happens even if caller doesn't subscribe
    result.subscribe( // triggers the operation, making it "hot"
      undefined, // only care about failure
      error => this.handleError(error)
    );

    return result;
  }

  deleteCustomer(id: number): Observable<Response> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.customersUrl}/${id}`;
    return this.http.delete(url, options)
      .do(data => console.log('deleteCustomer: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveCustomer(customer: ICustomer): Observable<ICustomer> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    if (customer.id === 0) {
      return this.createCustomer(customer, options);
    }
    return this.updateCustomer(customer, options);
  }

  private createCustomer(customer: ICustomer, options: RequestOptions): Observable<ICustomer> {

    customer.id = undefined;

    return this.http.post(this.customersUrl, customer, options)
      .map(this.extractData)
      .do(data => console.log('createCustomer: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private updateCustomer(customer: ICustomer, options: RequestOptions): Observable<ICustomer> {

    const url = `${this.customersUrl}/${customer.id}`;

    return this.http.put(url, customer, options)
      .map(() => customer)
      .do(data => console.log('updateCustomer: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    let body = response.json();
    return body.data || {};
  }

  private initializeCustomer(): ICustomer {
    // Return an initialized object
    return {
      id: 0,
      firstName: null,
      lastName: null,
      middleName: null,
      email: null,
      gender: null,
      company: null,
      phone: {
        mobile: null,
        work: null,
        home: null
      },
      address: {
        home: {
          postalCode: null,
          city: null,
          country: null,
          province: null,
          street: null
        },
        work: {
          postalCode: null,
          city: null,
          country: null,
          province: null,
          street: null
        }
      }
    };
  }

  /** Common Http Observable error handler */
  private handleError(error: any): Observable<any> {
    this.logger.log(`An error occurred: ${error}`); // for demo purposes only
    // re-throw user-facing message
    return Observable.throw('Something bad happened; please check the console');
  }
}