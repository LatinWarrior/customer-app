import { InMemoryDbService } from '../../../node_modules/angular-in-memory-web-api';

import { createTestCustomers } from '../resources/test-data';
import { testCustomers } from '../resources/customers';
import { testProducts } from '../resources/products';
import { testStates } from '../resources/states';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    // const states = ['California', 'Illinois', 'Jalisco', 'Quebec', 'Florida'];
    return { customers: testCustomers(), states: testStates(), products: testProducts() };
  }
}