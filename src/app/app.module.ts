import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Kendo UI
import { GridModule } from '@progress/kendo-angular-grid';
import { TabStripModule } from '@progress/kendo-angular-layout';
import { DropDownsModule, DropDownListModule } from '@progress/kendo-angular-dropdowns';

import { AppComponent } from './app.component';

// Services
import { DataService } from './services/data-service';
import { LoggerService } from './services/logger-service';
import { MessageService } from './services/message-service';
import { AuthService } from './services/auth-service';

import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found.component';
import { WelcomeComponent } from './home/welcome.component';

/* Features Module */
import { FeaturesModule } from './features/features.module';
/* Messages Module */
import { MessagesModule } from './messages/messages.module';

// in-mem-web-api and its test-data service
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './repository/in-memory-data.service';

// Google Map
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    PageNotFoundComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule, // <--  Built-in
    BrowserAnimationsModule, // Built-in
    FormsModule, // <--  Built-in
    CommonModule, // <--  Built-in
    HttpModule,  // <--  Built-in HttpModule
    GridModule, // <-- Kendo UI Grid
    TabStripModule, // <-- Kendo UI Tab STrip
    DropDownsModule, // <-- Kendo DropDown
    DropDownListModule, // <-- Kendo DropDownList
    AgmCoreModule.forRoot(),
    NgbModule.forRoot(), // Bootstrap
    FeaturesModule, // < App Features
    MessagesModule, // <-- App Messages
    AppRoutingModule, // <-- App Routing. NOTE: This must be placed after ALL Feature modules. The order is important.
    InMemoryWebApiModule.forRoot(InMemoryDataService) // <-- register in-mem-web-api and its data
  ],
  providers: [
    DataService,
    LoggerService,
    MessageService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
