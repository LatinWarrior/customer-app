import { Component } from '@angular/core';

@Component({
    template: `    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2>This is not the page you were looking for!</h2>
        </div>
        <div class="panel-body">                
            <div class="row" >
                <img [src]="'assets/images/404_page.jpg'" 
                    class="img-responsive center-block"
                    style="max-height:40.0rem;padding-bottom:1.0rem"/>
            </div>
        </div>
    </div>
    `
})
export class PageNotFoundComponent { }
