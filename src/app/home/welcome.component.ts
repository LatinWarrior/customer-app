import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: './welcome.component.html'
})
export class WelcomeComponent {
    public pageTitle: string = 'Welcome';
    public imagePath: string = './assets/images/logo.jpg';
}
