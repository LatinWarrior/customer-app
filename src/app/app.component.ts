import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth-service';
import { LoggerService } from './services/logger-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  currentDate = new Date().toLocaleString('en-US');

  constructor(private authService: AuthService, private router: Router, private logger: LoggerService) {

  }

  logout() {
    this.authService.logout();
    this.logger.log('successfully logged out');
    // Clean up the URL route by using the navigate by URL method.
    this.router.navigateByUrl('/welcome');
  }
}
