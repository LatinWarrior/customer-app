export interface ICustomer {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  gender: string;
  address: ICustomerAddress;
  phone: ICustomerPhone;
  company: string;
}

export interface ICustomerAddress {
  home: {
    street: string;
    city: string;
    province: string;
    postalCode: string;
    country: string;
  };
  work: {
    street: string;
    city: string;
    province: string;
    postalCode: string;
    country: string;
  };
}

export interface ICustomerPhone {
  mobile: string;
  home: string;
  work: string;
}
