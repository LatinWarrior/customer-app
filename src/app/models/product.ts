export interface IProductAddress {
        latitude: string;
        longitude: string;
        street: string;
        city: string;
        province: string;
        postalCode: string;
        country: string;
    }

    export interface IProductContact {
        firstName: string;
        lastName: string;
        phoneNumber: string;
    }

    export interface IProductDistributor {
        name: string;
        address: IProductAddress;
        contact: IProductContact;
    }

    export interface IProduct {
        id: number;
        name: string;
        cost: string;
        description: string;
        distributor: IProductDistributor;
    }
