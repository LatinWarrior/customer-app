import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found.component';
import { WelcomeComponent } from './home/welcome.component';
import { CompanyMapComponent } from './features/maps/company-map/company-map.component';

const ROUTES = [
    // { path: 'customers', component: CustomerListComponent },
    // { path: 'customers/:id', component: CustomerDetailComponent, resolve: { customer: CustomerResolver } },
    // { path: 'customers/:id/edit', component: CustomerEditComponent, resolve: { customer: CustomerResolver } },
    { path: 'welcome', component: WelcomeComponent },
    { path: '', redirectTo: 'welcome', pathMatch: 'full'},
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES),
    ],
    exports: [ RouterModule ]   // This is very important! It allows the components declared
                                            // in App module to have access to the Router module.
})
export class AppRoutingModule { }
